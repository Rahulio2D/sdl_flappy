# SDL_Flappy
SDL_Flappy is a basic clone of the popular mobile game Flappy Bird by [dotGears](http://dotgears.com/apps/app_flappy.html). This game was designed as a way to learn more about SDL/C++ by creating a game which incorporated basic collision detection, random generation, audio, and more.

## Installation
This project will unfortunately not run immediately after downloading the repository as I have not included the SDL Libraries. In order to get the project running, you will need to create a folder titled '_Libraries' within the root of the project, and in there place the latest development libraries for [SDL2](https://www.libsdl.org/download-2.0.php), [SDL2_image](https://www.libsdl.org/projects/SDL_image/) and [SDL2_ttf](https://www.libsdl.org/projects/SDL_ttf/) - after downloading be sure to remove the version number for each folder. Lastly, you will also have to move the x64 DLL files to the root of the project, these are located in the 'lib' folder of each folder you've added to the '_Library' folder. As this installation procedure is fairly complicated, I've also set up a folder on my Google Drive which contains all the Libraries and DLLs already set up, this can be downloaded [here](https://drive.google.com/file/d/1h0NIcE7dY-C451F_tqe73VUwYMie-oOC/view?usp=sharing).

## Credits
All the code within this project was written by me ([Rahul Sharma](https://www.rahulsharma.uk)), however, I did use some external assets for art and audio.
- [Flappy Bird Spritesheet](https://www.spriters-resource.com/fullview/59894/) - Spriters Resource
- [Pixellari Font](https://www.dafont.com/pixellari.font) - dafont
- [Sound Effects](https://www.101soundboards.com/boards/10178-flappy-bird-sounds) - 101 Soundboards
