#include "FloorObject.h"
#include "Time.h"

FloorObject::FloorObject()
{
	m_MoveSpeed = 2.5f;
}

FloorObject::~FloorObject()
{
}

void FloorObject::Initialise(Vector2 position, Vector2 size)
{
	sourceRect = SDL_Rect{ 298, 0, 48, 50 };
	GameObject::Initialise(position, size);
}

void FloorObject::Update() {
	m_Pos.x -= m_MoveSpeed * Time::GetDeltaTime();
	GameObject::Update();
}

void FloorObject::SetPosition(Vector2 position) {
	m_Pos = position;
	Update();
}
