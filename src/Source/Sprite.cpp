#include "Sprite.h"
#include "SDL.h"
#include "SDL_image.h"
#include "Game.h"

Sprite::Sprite(const char* spritePath) {
	SDL_Surface* surface = IMG_Load(spritePath);
	texture = SDL_CreateTextureFromSurface(Game::renderer, surface);
	SDL_FreeSurface(surface);
}