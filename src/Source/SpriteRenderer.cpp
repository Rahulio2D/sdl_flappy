#include "SpriteRenderer.h"
#include "Sprite.h"

//Initialise a sprite rendererer so an image can be loaded based on the image path and specified rects
void SpriteRenderer::Initialise(Sprite* sprite, SDL_Rect sourceRect) {
	this->sprite = sprite;
	this->sourceRect = sourceRect;
	flipX = flipY = false;
	renderSprite = true;
	colour = SDL_Color{ 255, 255, 255, 255 };
}

void SpriteRenderer::UpdateSourceRect(SDL_Rect source) {
	this->sourceRect = source;
}
