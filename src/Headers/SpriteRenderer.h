#ifndef SPRITERENDERER_H
#define SPRITERENDERER_H

#include "SDL.h"
#include "SDL_image.h"
#include "Sprite.h"

class SpriteRenderer {
public:
	void Initialise(Sprite* sprite, SDL_Rect source);
	void UpdateSourceRect(SDL_Rect source);

	Sprite* sprite;
	SDL_Rect sourceRect;
	bool flipX = false;
	bool flipY = false;
	bool renderSprite = true;
	SDL_Color colour;
};

#endif 