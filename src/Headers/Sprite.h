#ifndef SPRITE_H
#define SPRITE_H

class SDL_Texture;

class Sprite {
public:
  Sprite(const char* spritePath);

  SDL_Texture* texture;
};
#endif // !SPRITE_H
